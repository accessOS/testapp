var access = {
  createContextMenu: function(data,p_pid,offsetY,offsetX, from_app, context) {
    console.log(data,'P_pid',p_pid,offsetY,offsetX, from_app, context)
    var {ipcRenderer} = require('electron')
    ipcRenderer.send('request create context menu', {
      menu: data,
      pid: p_pid,
      offsetY: offsetY,
      offsetX: offsetX,
      from_app: from_app? true:false
    })
    ipcRenderer.send('context menu listener')
    ipcRenderer.on('callbacks init', function(e,callbacks){
      console.info("Callbacks init", e, callbacks)
      for (let i in callbacks) {
        console.log('Init callback', callbacks[i])
        ipcRenderer.on('click-callback-'+callbacks[i], function(){console.log("Callback:",callbacks[i]);data[i].callback.bind(context)()}.bind(this))
      }
    }.bind(this))
    // s.close()
    console.log("Create menu")
  },

  registerContextMenuListener: function(callback) {
    var {ipcRenderer} = require('electron')
    console.log("Register")
    ipcRenderer.send('context menu listener')
    ipcRenderer.on('create context menu',function(e, data){
      for (let i in data.menu) {
        let v = data.menu[i]
        v.callback = function(){ipcRenderer.send('callback click', data.callbacks[i])}.bind(this)
      }
      callback(data)
      console.log("Gotcha")
    })
  },
}

module.exports = access
